import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(CreateWeatherApp());
}

class CreateWeatherApp extends StatefulWidget {
  @override
  State<CreateWeatherApp> createState() => _CreateWeatherApp();
}

class _CreateWeatherApp extends State<CreateWeatherApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.blueAccent,
          leading: Icon(Icons.arrow_back),
          title: Text(
            "Weather Forecast",
            style: TextStyle(fontWeight: FontWeight.w600),
          ),
        ),
        backgroundColor: Colors.blue.shade100,
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: NetworkImage(
                  'https://cdn6.f-cdn.com/contestentries/329593/14489232/569add2d8d3cb_thumb900.jpg'),
              fit: BoxFit.cover,
              opacity: 0.5,
            ),
          ),
          child: ListView(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(top: 40.0),
                    child: Text(
                      "Bang Bua Thong",
                      style: TextStyle(
                        fontSize: 30,
                        fontWeight: FontWeight.bold,
                        color: Colors.black26.withOpacity(0.8),
                        shadows: [
                          Shadow(
                            color: Colors.black26,
                            blurRadius: 25,
                            offset: Offset(5.0, 3.0),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(top: 10.0),
                    child: Text(
                      "28°",
                      style: TextStyle(
                        fontSize: 70,
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                        shadows: [
                          Shadow(
                            color: Colors.black26,
                            blurRadius: 25,
                            offset: Offset(3.0, 5.0),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    "Partly Cloudy",
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.w600),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 10.0),
                  ),
                  Image.asset(
                    'images/partly-cloudy.png',
                    width: 50,
                    height: 50,
                  ),
                ],
              ),
              Padding(
                padding: EdgeInsets.only(bottom: 30),
              ),
              Divider(
                color: Colors.white,
                thickness: 0.3,
              ),
              Container(
                child: Row(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(left: 15),
                    ),
                    Container(
                      child: Text(
                        "Today",
                        style: TextStyle(
                            fontSize: 22, fontWeight: FontWeight.w500),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(right: 50),
                    ),
                    Container(
                      child: Image.asset(
                        'images/partly-cloudy.png',
                        width: 50,
                        height: 50,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(right: 50),
                    ),
                    Container(
                      child: Image.asset(
                        'images/rainy-night.png',
                        width: 50,
                        height: 50,
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(right: 20.0, left: 20.0),
                        ),
                        Container(
                          child: Text(
                            "28°",
                            style: TextStyle(
                                fontSize: 22, fontWeight: FontWeight.w500),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(left: 10.0),
                          child: Text(
                            "25°",
                            style: TextStyle(
                                fontSize: 22, fontWeight: FontWeight.w500),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(bottom: 20),
              ),
              Divider(
                color: Colors.white,
                thickness: 0.5,
              ),
              //Tuesday Row
              Container(
                child: Row(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(left: 15),
                    ),
                    Container(
                      child: Text(
                        "Tuesday",
                        style: TextStyle(
                            fontSize: 22, fontWeight: FontWeight.w500),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(right: 26),
                    ),
                    Container(
                      child: Image.asset(
                        'images/icons8-sun-96-removebg-preview.png',
                        width: 50,
                        height: 50,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(right: 50),
                    ),
                    Container(
                      child: Image.asset(
                        'images/partly-cloudy.png',
                        width: 50,
                        height: 50,
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(right: 20.0, left: 20.0),
                        ),
                        Container(
                          child: Text(
                            "31°",
                            style: TextStyle(
                                fontSize: 22, fontWeight: FontWeight.w500),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(left: 10.0),
                          child: Text(
                            "28°",
                            style: TextStyle(
                                fontSize: 22, fontWeight: FontWeight.w500),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(bottom: 20),
              ),
              Divider(
                color: Colors.white,
                thickness: 0.6,
              ),
              //Wenesday Row
              Container(
                child: Row(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(left: 15),
                    ),
                    Container(
                      child: Text(
                        "Wednesday",
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.w500),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(right: 5),
                    ),
                    Container(
                      child: Image.asset(
                        'images/rainy-night.png',
                        width: 50,
                        height: 50,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(right: 50),
                    ),
                    Container(
                      child: Image.asset(
                        'images/partly-cloudy.png',
                        width: 50,
                        height: 50,
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(right: 20.0, left: 20.0),
                        ),
                        Container(
                          child: Text(
                            "25°",
                            style: TextStyle(
                                fontSize: 22, fontWeight: FontWeight.w500),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(left: 10.0),
                          child: Text(
                            "28°",
                            style: TextStyle(
                                fontSize: 22, fontWeight: FontWeight.w500),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(bottom: 20),
              ),
              Divider(
                color: Colors.white,
                thickness: 0.6,
              ),
              //Thursday Row
              Container(
                child: Row(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(left: 15),
                    ),
                    Container(
                      child: Text(
                        "Thursday",
                        style: TextStyle(
                            fontSize: 22, fontWeight: FontWeight.w500),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(right: 19),
                    ),
                    Container(
                      child: Image.asset(
                        'images/rainy-night.png',
                        width: 50,
                        height: 50,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(right: 50),
                    ),
                    Container(
                      child: Image.asset(
                        'images/icons8-night-wind-96-removebg-preview.png',
                        width: 50,
                        height: 50,
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(right: 20.0, left: 20.0),
                        ),
                        Container(
                          child: Text(
                            "25°",
                            style: TextStyle(
                                fontSize: 22, fontWeight: FontWeight.w500),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(left: 10.0),
                          child: Text(
                            "24°",
                            style: TextStyle(
                                fontSize: 22, fontWeight: FontWeight.w500),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(bottom: 20),
              ),
              Divider(
                color: Colors.white,
                thickness: 0.6,
              ),
              //Friday Row
              Container(
                child: Row(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(left: 15),
                    ),
                    Container(
                      child: Text(
                        "Friday",
                        style: TextStyle(
                            fontSize: 22, fontWeight: FontWeight.w500),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(right: 50),
                    ),
                    Container(
                      child: Image.asset(
                        'images/icons8-storm-96-removebg-preview.png',
                        width: 50,
                        height: 50,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(right: 50),
                    ),
                    Container(
                      child: Image.asset(
                        'images/icons8-night-wind-96-removebg-preview.png',
                        width: 50,
                        height: 50,
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(right: 20.0, left: 20.0),
                        ),
                        Container(
                          child: Text(
                            "23°",
                            style: TextStyle(
                                fontSize: 22, fontWeight: FontWeight.w500),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(left: 10.0),
                          child: Text(
                            "24°",
                            style: TextStyle(
                                fontSize: 22, fontWeight: FontWeight.w500),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(bottom: 20),
              ),
              Divider(
                color: Colors.white,
                thickness: 0.6,
              ),
              //Saturday
              Container(
                child: Row(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(left: 15),
                    ),
                    Container(
                      child: Text(
                        "Saturday",
                        style: TextStyle(
                            fontSize: 22, fontWeight: FontWeight.w500),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(right: 26),
                    ),
                    Container(
                      child: Image.asset(
                        'images/icons8-sun-96-removebg-preview.png',
                        width: 50,
                        height: 50,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(right: 50),
                    ),
                    Container(
                      child: Image.asset(
                        'images/icons8-storm-96-removebg-preview.png',
                        width: 50,
                        height: 50,
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(right: 20.0, left: 20.0),
                        ),
                        Container(
                          child: Text(
                            "31°",
                            style: TextStyle(
                                fontSize: 22, fontWeight: FontWeight.w500),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(left: 10.0),
                          child: Text(
                            "24°",
                            style: TextStyle(
                                fontSize: 22, fontWeight: FontWeight.w500),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(bottom: 20),
              ),
              Divider(
                color: Colors.white,
                thickness: 0.6,
              ),
              //Saturday
              Container(
                child: Row(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(left: 15),
                    ),
                    Container(
                      child: Text(
                        "Sunday",
                        style: TextStyle(
                            fontSize: 22, fontWeight: FontWeight.w500),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(right: 42),
                    ),
                    Container(
                      child: Image.asset(
                        'images/partly-cloudy.png',
                        width: 50,
                        height: 50,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(right: 50),
                    ),
                    Container(
                      child: Image.asset(
                        'images/icons8-storm-96-removebg-preview.png',
                        width: 50,
                        height: 50,
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(right: 20.0, left: 20.0),
                        ),
                        Container(
                          child: Text(
                            "28°",
                            style: TextStyle(
                                fontSize: 22, fontWeight: FontWeight.w500),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(left: 10.0),
                          child: Text(
                            "24°",
                            style: TextStyle(
                                fontSize: 22, fontWeight: FontWeight.w500),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(bottom: 20),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
